#ifndef TEXTUREMANAGER_H
#define TEXTUREMANAGER_H

#include <assert.h>

template <typename T>
class Singleton
{
    static T* ms_Singleton;	//jedyny obiekt

public:
    Singleton( void )
    {
        assert( !ms_Singleton );	//sprawdz, czy nie probujemy stworzyc Singletona poraz drugi

	//oblicza adres Singletona
        int offset = (int)(T*)1 - (int)(Singleton *)(T*)1;
        ms_Singleton = (T*)((int)this + offset);
    }
   ~Singleton( void )
        {  assert( ms_Singleton );  ms_Singleton = 0;  }
    static T& GetSingleton( void )
        {  assert( ms_Singleton );  return ( *ms_Singleton );  }
    static T* GetSingletonPtr( void )
        {  return ( ms_Singleton );  }
};

//zmienna statyczna
template <typename T> T* Singleton <T>::ms_Singleton = 0;

#endif // TEXTUREMANAGER_H
