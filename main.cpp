#include <iostream>
#include <string>

class A {
    public:
        A(){;}
        std::string foo() {
            return "A class";
        }
};

class B: A {
    public:
        B(){;}
        std::string foo() {
            return "B class";
        }
};

int fooStatic() {
    static int a = 0;
    return a++;
}

void funcFoo() {
    int i = 5;
    void *prt = &i;
    int *k = static_cast<int*>(prt);
    std::cout << prt << " " << &i << " " << *k << std::endl;
}

int main() {
    B obj;
    std::cout << obj.foo() << std::endl;
    std::string napis;
    std::cout << (napis = "jakis napis");;
    return 0;
}
